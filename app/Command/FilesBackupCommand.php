<?php
namespace App\Command;

use App\Command\BaseCommand;
use mysql_xdevapi\Exception;

class FilesBackupCommand extends BaseCommand {
    
    public $nameCommand = "files_backup";
    protected $requiredEnvVariables = ["FILES_FOLDER", "EXCLUDE_FILES", "EMAIL", "EMAIL_COPY", "SMTP_HOST", "SMTP_USERNAME", "SMTP_PASSWORD", "SMTP_SECURE", "SMTP_PORT", "EMAIL_FROM", "SERVER_NAME", "WEBDAV_TOKEN", "WEBDAV_MININMUM_FREE_SPACE_GB"];
    protected $baseFolderForEnv = 'WEBDAV_FILES_FOLDER';
    protected $pathForBackup;
    
    public function __construct()
    {   
        parent::__construct();
    }
    
    public function run() 
    {
        //сохдаем папку для бэкапа
        $pathToUpload = $this->createFolderForUpload($this->baseFolderForCommandOnYandexDisk, $this->date);

        foreach ($this->config->files as $item) {

            $excludeString = '';
            foreach ($item->excluded as $excludedItem) {
                $excludeString .= "--exclude \"{$excludedItem}\" ";
            }

            $pathForNewArchive = $this->pathForBackup . basename($item->entry) . '.tar.gz';
            $command = "tar czf {$pathForNewArchive} {$excludeString} {$item->entry} --warning=no-file-changed";

            //создаем архив
            exec($command);

            //определяем размер архива, чтобы освоболить под него место на яндекс диске
            $fileSize = filesize($pathForNewArchive);
            //определяем свобоное место на диске
            $this->checkFreeSpaceOnYandexDisk($fileSize, $this->WEBDAV_MININMUM_FREE_SPACE_GB, $this->baseFolderForCommandOnYandexDisk);
            //загружаем архив дампа на яндекс диск
            $this->uploadToYandexDisk($pathForNewArchive, $pathToUpload);
            //удаляем архив с дампом с сервера
            unlink($pathForNewArchive);
        }

        $this->sendEmailWithLog("Бэкап файлов создан");
    }
}
